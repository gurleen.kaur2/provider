package com.provider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Output {
    private Operation operation;
    private int result;
    private int invertedResult;
}
