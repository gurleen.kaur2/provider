package com.provider.controller;

import com.provider.model.Input;
import com.provider.model.Operation;
import com.provider.model.Output;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/calculator")
public class CalculatorController {
    @PostMapping("/add")
    @io.swagger.v3.oas.annotations.Operation(
        tags = {"Calculator"},
        responses = {
            @ApiResponse(
                responseCode = "200",
                description = "Returns the value",
                content = {
                    @Content(
                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                        schema = @Schema(implementation = Output.class))
                }),
        })
    public ResponseEntity<Output> add(@RequestBody Input input) {
        int addResult = input.getNumber1() + input.getNumber2();
        Output output = new Output(Operation.ADD, addResult, addResult * -1);
        return ResponseEntity.ok(output);
    }
}
